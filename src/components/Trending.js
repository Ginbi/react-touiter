import React from 'react';
import TouitAPI from "../api/TouitAPI";

class Trending extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            "words": []
        }
    }

    componentDidMount() {
        TouitAPI.getTrending(callback => {
            this.setState({
                "words": Object.entries(callback)
                    .map(([k, v]) => ({"key": k, "value": v}))
                    .sort((a, b) => b.value - a.value)
            });
        });
    }

    render(){
        const {words} = this.state;
        return(
            <div className="trending">
                <h2>ТЯЕИDIИG</h2>
                <div className="trending-box">
                {
                    words.map(word => <div className="trending-word" key={word.key}>{word.key}</div>)
                }
                </div>
            </div>
        )
    }
}

export default Trending;
