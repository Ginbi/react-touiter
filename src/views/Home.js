import React from 'react';

import SendMessageForm from '../components/SendMessageForm';
import TouitContainer from '../components/TouitContainer';

class Home extends React.Component{
    render(){
        return(
            <>
                <SendMessageForm />
                <TouitContainer />
            </>
        )
    }
}

export default Home;