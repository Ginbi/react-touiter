import './App.css';
import './Reset.css';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import Header from './components/Header';
import Home from './views/Home'
import TrendingPage from './views/TrendingPage';

function App() {
  return (
      <Router>
        <div>
            <Header />
            <main>
                <div className="container">
                    <Route exact path="/" component={Home}/>
                    <Route path="/trending" component={TrendingPage} />
                </div>
            </main>
        </div>
    </Router>
  );
}

export default App;