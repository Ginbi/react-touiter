import React from 'react';
import Touit from './Touit';
import TouitAPI from '../api/TouitAPI';


class TouitContainer extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            "touits":[]
        };
        this.lastTimestamp = 0;
        this.intervalID = false;
    }


refresh = () => {
            TouitAPI.getTwits(this.lastTimestamp, response => {
                if (response.messages.length > 0) {
                    this.setState({
                        "touits" : [...this.state.touits, ...response.messages]
                    });
                }
                this.lastTimestamp = response.ts;
            });
        }


    componentDidMount(){
        this.intervalID = setInterval(this.refresh, 1000);
    }

    componentWillUnmount(){
        if (this.intervalID !== false){
        clearInterval(this.intervalID);
        }
    }

    render(){
        const {touits} = this.state;
        return (
            <div className="right-side">
                <h2>Dегиіегѕ тоцітѕ</h2>
                    <div className="twit-container">
                    {
                        touits
                        .sort((a, b) => b.ts - a.ts)
                        .map(t => <Touit {...t} key={t.id} />)
                    }
                    
                </div>
            </div>
        )
    }
}

export default TouitContainer;
