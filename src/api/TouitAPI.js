class TouitAPI {

    static getTrending(callback) {

        const request = new XMLHttpRequest();

        request.open("GET", "http://touiteur.cefim-formation.org/trending")

        request.addEventListener("readystatechange", function () {
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status === 200) {
                    const response = JSON.parse(request.responseText);
                    callback(response);
                }
            } else {
                // TODO: error message
            }
        });
    
        request.send();
    }

    // static getTrending = async (refresh) => {
    //     const response = await fetch('http://touiteur.cefim-formation.org/trending');
    //     let data = await response.json();
    //     return data;
    // }


    static getTwits(timestamp, callback) {

	const request = new XMLHttpRequest();

	request.open("GET", "http://touiteur.cefim-formation.org/list?ts=" + encodeURIComponent(timestamp), true);

	request.addEventListener("readystatechange", function () {
		if (request.readyState === XMLHttpRequest.DONE) {
			if (request.status === 200) {
                const response = JSON.parse(request.responseText);
                callback(response);
			}
		} else {
			// TODO: error message
		}
	});

	request.send();
}

static sendTwit(name, msg) {
	const data =
		"name=" +
		encodeURIComponent(name) +
		"&message=" +
		encodeURIComponent(msg);
	const request = new XMLHttpRequest();

	request.open("POST", "http://touiteur.cefim-formation.org/send", true);

	request.setRequestHeader(
		"Content-type",
		"application/x-www-form-urlencoded"
	);

	request.send(data);
}



}

export default TouitAPI;
