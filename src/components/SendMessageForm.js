import React from 'react';
import TouitAPI from '../api/TouitAPI';

class SendMessageForm extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            "username":"",
            "twit":""
        }
    }

handleSubmit = (e) => {
    e.preventDefault()
    const {username, twit} = this.state;
    TouitAPI.sendTwit(username, twit);

    this.setState({
        "username":"",
        "twit":""
    })
}

getValue = (ev) =>{
    this.setState({
        [ev.target.name]: ev.target.value
    })
}

    render(){
        const {username, twit} = this.state;
        return (
            <div className="left-side">
                <form action="#" autocomplete="off" className="twit-block" onSubmit={this.handleSubmit}>
                    <div className="username-input">
                        <label htmlFor="username">Д</label>
                            <input type="text" id="username" name="username" minLength="3" maxLength="16" value={username}onChange={this.getValue}/>
                    </div>
                    <div className="twit-input">
                        <label htmlFor="twit">ТОЦІТ</label>
                            <textarea type="textarea" id="twit" name="twit" minLength="3" maxLength="256" value={twit} onChange={this.getValue}></textarea>
                    </div>
                    <input type="submit" value="Еиvоуег" id="send-btn" />
                    <a href="#top" className="BTT-btn">Яетоцг</a>
                </form>  
            </div>
        )
    }
}

export default SendMessageForm;