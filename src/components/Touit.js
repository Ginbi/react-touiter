import React from 'react';

class Touit extends React.Component{
    render(){
        const {message, name} = this.props;
        return (
                <div className="twit-card">
                    <p className="twit-content">{message}</p>
                    <p className="twit-poster">Camarade "{name}"</p>
                </div>
        )
    }
}

export default Touit;