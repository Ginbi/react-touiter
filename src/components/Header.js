import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import logo from '../assets/img/logo.png';

class Header extends React.Component{
    render(){
        return (
            <header>
                <a id="top"></a>
                <div className="container">
                    <img src={logo} height="120px"/>
                    <h1>Sovieтоціт</h1>
                    <nav>
                        <ul>
                            <li><Link to="/">НФМЕ</Link></li>
                            <li><Link to="/trending">ТЯЕИDІИG</Link> </li>
                        </ul>
                    </nav>
                </div>
            </header>
        )
    }
}

export default Header;